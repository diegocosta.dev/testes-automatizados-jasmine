import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';

xdescribe('AppComponent', () => {
beforeEach(async(() => {
  TestBed.configureTestingModule({
    imports: [
      RouterTestingModule
    ],
    declarations: [
      AppComponent
    ],
  }).compileComponents();
}));

it('should create the app', () => {
  const fixture = TestBed.createComponent(AppComponent);
  const app = fixture.debugElement.componentInstance;
  expect(app).toBeTruthy();
});

it(`should have as title 'estudoAngular'`, () => {
  const fixture = TestBed.createComponent(AppComponent);
  const app = fixture.debugElement.componentInstance;
  expect(app.title).toEqual('estudoAngular');
});

it('should render title', () => {
  const fixture = TestBed.createComponent(AppComponent);
  fixture.detectChanges();
  const compiled = fixture.debugElement.nativeElement;
  expect(compiled.querySelector('.content span').textContent).toContain('estudoAngular app is running!');
});
});


// toBe
// realiza a comparação com o operador '===' que compara o valor e também o tipo objeto
// deve ser utilizado para comparar valores d forma mais efetivapelo fato de tabem verificar o tipo de objeto
describe('Comparador toBe', () => {
it('deve validar o uso do toBe', () =>{
  expect(true).toBe(true); // boolean
  expect('teste').toBe('teste'); // string
});

it('espero que compare os valores de um obejto', () => {
  // criando objeto para testes (esse teste vai gerar um erro)
  const value1 = {value: true};
  const value2 = {value: true};
  expect(value1).toBe(value1); // true, pois compara os objetos, não os valores
  expect(value1).not.toBe(value2); // isso prova que a verificação é no objeto
  //expect(value1).toBe(value2); // vai gerar erro, pois o toBe não vai verificar os valores e sim se são o mesmo objeto (independente dos valores)
});
});

// toEquals
describe('Comparador toEqual', () => {
it('deve validar o uso do toEqual', () => {
  expect(true).toEqual(true);
});
it('deve validar o valor de dois objetos diferentes', () => {
  // criando objeto para testes
  const value1 = {value: true};
  const value2 = {value: true};
  expect(value1).toEqual(value2); //neste caso conseguimos comparar os valores de um objeto e não os objetos em sí    
});
});

// faz comparações usando expressões regulares
describe('Comparador toMatch', () => {
const texto = 'teste com Jasmine';
// caso seja passando uma string como parâmetro, o camparador irá TENTAR compara o texto passado
it('espero que compare string', ()=> {
  expect(texto).toMatch('teste com Jasmine');
});

it('espero que compare valores com expressões regulares', ()=> {
  // ele é case sentive na comparação de strings
  expect(texto).toMatch(/jasmine/i); // usando expressão regular, teste caso passei o 'i', para dizer que a expressão é case insensitive
  expect('12345-678').toMatch(/^\d{5}-\d{3}$/);// valida cep 
  expect('21-12345-6789').toMatch(/^[0-9]{2}-[0-9]{5}-[0-9]{4}$/);// valida telefone
});
});

// realiza a comparação de um objeto como sendo NÃO UNDEFINED
// prefira usar o tobeUndefined ao invés de not.toBeDefined
describe('Comparador toBeDefined', () => {
const n1 = 10;
let n2; // undefined
let n3 = undefined; // undefined

it('espero demonstrar o uso do toBeDefined', () =>{
  expect(n1).toBeDefined(); // true
  expect(null).toBeDefined();   // true porque NULL é um tipo no javascript
  expect(NaN).toBeDefined();    // true porque NaN é um tipo no javascript

  // undefined
  expect(n2).not.toBeDefined(); // adicionado not par aque o teste passe pois o valor é undefined
  expect(n3).not.toBeDefined(); // adicionado not par aque o teste passe pois o valor é undefined
});
});


// realiza a comparação de um objeto como sendo UNDEFINED
// prefira usar o tobeDefined ao invés de not.toBeUnefined
describe('Comparador toBeUndefined', () => {
const n1 = 10;
let n2; // undefined
let n3 = false; // defined porque false é um booleano

it('espero demonstrar o uso do toBeUndefined', () =>{
  expect(n1).not.toBeUndefined();
  expect(n2).toBeUndefined();
  expect(n3).not.toBeUndefined();
  expect(null).not.toBeUndefined();
});
});

// realiza a comparação dizendo se uma variável ou objeto possui um valor válido.
// um valor será considerado válido caso ele possua um valor diferente de 'false', '0', '', 'undefined', 'null' ou 'NaN' (essas regras são do javascript, paa validação de objetos)
// deve ser utilizado quando a verificação aborda valores inválidos distintos
// prefira utilizar o toBeFalsy ao invés de not.toBeTruthy para deixar o código mais legível
describe('Comparador toBeTruthy', ()=>{
const n1 = false;
const n2 = 0;
const n3 = '';
const n4 = null;
const n5 = NaN;

const n6 = 10;

it('espero demonstrar o uso do toBeTrhuthy', () => {
  expect(n1).not.toBeTruthy();
  expect(n2).not.toBeTruthy();
  expect(n3).not.toBeTruthy();
  expect(n4).not.toBeTruthy();
  expect(n5).not.toBeTruthy();

  // exemplos de de true 
  expect(n6).toBeTruthy();
  expect(true).toBeTruthy();
  expect('teste').toBeTruthy();
});

});

// realiza a comparação dizendo se uma variável ou objeto possui um valor inválido.
// um valor será considerado inválido caso ele possua um valor igual a: 'false', '0', '', 'undefined', 'null' ou 'NaN' (essas regras são do javascript, paa validação de objetos)
// deve ser utilizado quando a verificação aborda valores inválidos distintos
// prefira utilizar o toBeTruthy ao invés de not.toBeFalsy para deixar o código mais legível
describe('Comparador toBeFalsy', ()=>{
const n1 = false;
const n2 = 0;
const n3 = '';
const n4 = null;
const n5 = NaN;

const n6 = 10;

it('espero demonstrar o uso do toBeFalsy', () => {
  expect(n1).toBeFalsy();
  expect(n2).toBeFalsy();
  expect(n3).toBeFalsy();
  expect(n4).toBeFalsy();
  expect(n5).toBeFalsy();

  // exemplos de de true 
  expect(n6).not.toBeFalsy();
  expect(true).not.toBeFalsy();
  expect('teste').not.toBeFalsy();
});
});

/*
*   Realiza a busca por um determinando item num array
*   Também pode ser utlizado para bsucar uma substring dentro de uma string
*   Não suporta busca por expressões regulares como o toMatch
*   ELE É CASE SENSITIVE
*/  
describe('Comparador toContain', () => {
const string = 'hello world';
let nomes = ['diego', 'damasceno', 'costa'];
it('espero demonstrar o uso do toContain num array', () => {
  expect(nomes).toContain('diego');
  expect(nomes).toContain('diego','costa');

  expect(nomes).not.toContain('Diego','costa'); // ele é case sensitive não temos 'Diego' com D maiúsculo
  expect(nomes).not.toContain('Raquel'); // não temos Raquel no array de nomes
});

it('espero demonstrar o uso do toContain em string', () => {
  expect(string).toContain('world')
});
});

/*
* Compara se um valor numérico é menos que o outro
* Realiza a conversão para o valor numérico antes de fazer a comparação, podendo ter o valor passado em formato de texto.
* Prefira usar o toBeLessThan ao invés do not.toBeGreaterThan
* PARA VALORES IGUAIS UTILIZE O toEqual
*/
describe('Comparador toBeLessThan', () => {
const PI = 3.1415;

it('espero demonstar o uso do toBeLessThan', () => {
  expect(3).toBeLessThan(PI);
  expect(3.15).not.toBeLessThan(PI);
  expect('3.15').not.toBeLessThan(PI); // passado como string  
});
});

/*
* Compara se um valor numérico é maior que o outro
* Realiza a conversão para o valor numérico antes de fazer a comparação, podendo ter o valor passado em formato de texto.
* Prefira usar o toBeGreaterThan ao invés do not.toBeLessThan
* PARA VALORES IGUAIS UTILIZE O toEqual
*/
describe('Comparador toBeGreaterThan', () => {
const VALUE = 5;

it('espero demonstar o uso do toBeGreaterThan', () => {
  expect(3).not.toBeGreaterThan(VALUE);
  expect(8).toBeGreaterThan(VALUE);
  expect('8').toBeGreaterThan(VALUE); // passado como string  
});
});

/**
 * verifica se uma excessão é lancada por uma método
 * Não realiza a validação em detalhes nem o tipo da exceção lançada, apenas verifica se aconteceu um erro na execução da função
 * DEVE SER UTILIZADA QUANDO DESEJA APENAS PARA CERTIFICAR QUE UM ERRO OCORREU. Sem se preocupar com o detalhes como tipo ou mensagem de erro,   
 */


// VERIFICAR ESSE TESTE, AINDA NÃO ENTENDI MUITO BEM
// esse teste não está funcinando muito bem
xdescribe('Comparador toThrow', ()=>{
  it('espero demonstrar o uso do Throw', () =>{
    var numero;
    // não retorna uma exceção (erro)
    // então esse teste vai falhar
    var somar = () => {
      return 10 + 10
    };
    // esse teste retorna uma exceção
    var multiplicar = () =>{
      return numero * 10
    };

    expect(somar).not.toThrow();
    expect(multiplicar).toThrow();
  });
});

/** verifica se há uma exceção lançada por um método
* valida o tipo de exceção lançada
* suporta expressão regulares na validação
* deve ser utilizada para maior controle do erro
*/
describe('Comparador toThrowError', () => {
let somar = (n1, n2) =>{
  if(n1 <= 0 || n2 <= 0){
    throw new TypeError("Deve ser maior que zero");
  }
  return n1 + n2;
}; 

it('espero demonstrar o uso do toThrow', () => {
  /**
   *  para testar a function mock, vc deve passa-la dentro de outra function
   *  expect(function(){ somar(0,0) }).toThrowError()
   */
  expect(()=>somar(0,0)).toThrowError();
  expect(()=>somar(0,0)).toThrowError('Deve ser maior que zero'); // atenção a mensagem de erro é case sensitive 
  expect(()=>somar(0,0)).toThrowError(/maior que zero/);
  expect(()=>somar(0,0)).toThrowError(TypeError);
  expect(()=>somar(0,0)).toThrowError(TypeError, 'Deve ser maior que zero');
  expect(()=>somar(10,2)).not.toThrowError();  
});
});

/**
* Falha manual permite interromper um teste, lançando um erro
* o Jasmine tem uma função Fail() para falhar manualmente um teste
* utilizamos a falha manual para certificar que uma operação não desejada, não seja executada 
*/
describe('Teste do Fail', () => {
it('Deve demonstrar o uso do fail', () => {
  
  let operacao = (executar, callback) => {
    if(executar){
      callback();
    }
  }

  // chamada do método
  operacao(false, () => {
    fail('não deve executar essa função de callback');
  });
});
});

/**
* Função javascript global do Jasmine que pe executada antes de cada teste
* Por ser executada antes de cada teste, serve para inicializar ou reiniciar um status
* Também pode iniciar uma ação antes de cada teste
*/

describe('beforEach', () => {
  var _count = 0

  beforeEach(async()=>{
    _count++;
  });

  it('espero incrementar o count em +1', () => {
    expect(_count).toEqual(1);
  });

  it('espero incrementar o count em +2', () => {
    expect(_count).toEqual(2);
  });
});

/**
* Função javascript global do Jasmine que pe executada depois de cada teste
* Por ser executada depois de cada teste, serve para inicializar ou reiniciar um status
* Também pode iniciar uma ação depois de cada teste
*/

describe('afterEach', () => {
  
  let count = 0;

  //antes de cada teste
  beforeEach(()=>{
    count++;
  });
  // depois de cada teste
  afterEach(()=>{
    count = 0
  });

  it('espero que mude o valor do contador para 1', () => {
    expect(count).toEqual(1);
  });

  it('Espero que mantenha o valor do contador em 1', () => {
    expect(count).toEqual(1);
  });
});

/**
* Função javascript global do Jasmine  que é executada uma única vez ANTES DA EXECUÇÃO DOS TESTES
* Por ser executada antes de todos os testes, serve para inicializar um status, criar objetos
*/
describe('beforAll', () => {
  
  let count = 0;

  beforeAll(()=> {
    count = 10;
  });

  beforeEach(()=> {
    count++;
  });

  it('espero iniciar com o valor de 11 para o contador', () => {
    expect(count).toEqual(11);
  });

  it('espero iniciar com o valor de 12 para o contador', () => {
    expect(count).toEqual(12);
  });

});

/**
* Função javascript global do Jasmine que é executada uma única vez DEPOIS DA EXECUÇÃO DOS TESTES
* Por ser executada depois de todos os testes, serve para inicializar um status, criar objetos
*/

describe('afterAll', () => {
let count = 0;

beforeAll(() => {
  count = 10;
});

afterAll(() => {
  count = 0;
});

it('espero iniciar com o valor de 10 para o contador', () => {
  expect(count).toEqual(10);
});
});


/**
* Aninhamento de suites
*/
describe('Suíte externa', () => {
var contadorExterno = 0;
beforeEach(() => {
  contadorExterno++;
});

it('deve conter 1 para contador externo', () => {
  expect(contadorExterno).toEqual(1);
});

  describe('Suíte interna',() => {
    var contadorInterno = 0;

    beforeEach(()=>{
      contadorInterno++;
    });

    it('deve validar o valor DOS CONTADORES', () => {
      expect(contadorInterno).toEqual(1);
      expect(contadorExterno).toEqual(2);
    });
  });
});

/**                                   
*             ------------- SPIES -------------  
* 
* Spies (ou mocks objects) são objetos falsos utilizados queremos manipular
* algum retorno que não faça parte dos testes em sí.
* Spies são utilizados para isolar somente o bloco de código que estamos testando.
* Spies somente poderão ser criados dentro dos blocos "describe" e "it".
* Spies são removidos ao término da execução da suíte.
*/

/**
  * spyOn serve para criar um mock, objeto falso, para ser utilizado nos testes.
  * Um pbjeto spy contém uma série de atributos
  * A função spyOn recebe como parâmetro o nome do objeto e do método a serem  utilizados como mock.
  * 
  */
describe('Testes do obejto spyOn', () => {
  //mock sem o spyOn ele retornaria o valor
  let Calculadora = {
    somar: (n1, n2) => {
      return n1 + n2;
    }
  };
  // é chamado uma única vez antes de  iniciar os testes
  beforeAll(() => {
    spyOn(Calculadora, "somar");
  });

  // importante do objeto spy quando é criado, já é inicializado automaticamente como undefined
  it('deve mostrar spyOn como undefined', () => {
    expect(Calculadora.somar(1,2)).toBeUndefined();
  });

});

/**
  * toHaveBeenCalled serve para saber se um método spy foi executado pelo menos 1 vez
  * toHaveBeenCalled, não possue parâmetros
  */

 describe('toHaveBeenCalled', () => {
  //mock sem o spyOn ele retornaria o valor
  let Calculadora = {
    somar: (n1, n2) => {
      return n1 + n2;
    }
  };
  // é chamado uma única vez antes de  iniciar os testes
  beforeAll(() => {
    spyOn(Calculadora, "somar");
  });

  // importante do objeto spy quando é criado, já é inicializado automaticamente como undefined
  it('deve chamar o método somar pelo menos 1 vez', () => {
    // importante: expect aqui apenas para fins de estudo.
    expect(Calculadora.somar).not.toHaveBeenCalled(); 
    // chama o método
    Calculadora.somar(1,1);
    //verifica se foi se foi chamado
    expect(Calculadora.somar).toHaveBeenCalled(); // não usei os parenteses no somar(), pois apenas quero chamar e não executar
  });
});

/**
  * toHaveBeenCalledTimes serve para verificar quantas vezes o método foi chamado
  * Recebe como parâmetro o número de vezes que deve ser verificado
  */

 describe('toHaveBeenCalled', () => {
  //mock sem o spyOn ele retornaria o valor
  let Calculadora = {
    somar: (n1, n2) => {
      return n1 + n2;
    }
  };
  // é chamado uma única vez antes de  iniciar os testes
  beforeAll(() => {
    spyOn(Calculadora, "somar");
  });

  // importante do objeto spy quando é criado, já é inicializado automaticamente como undefined
  it('deve chamar o método somar pelo menos 1 vez', () => {
    // chama o método
    Calculadora.somar(1,1);
    Calculadora.somar(2,2);
    //verifica quantas vezes o método foi chamado
    expect(Calculadora.somar).toHaveBeenCalledTimes(2); 
  });
});

/**
  * toHaveBeenCalledWith serve para verificar com quais parâmetros o método spy foi chamado
  * Recebe como parâmetro os valores do método spy
  */

 describe('toHaveBeenCalled', () => {
  //mock sem o spyOn ele retornaria o valor
  let Calculadora = {
    somar: (n1, n2) => {
      return n1 + n2;
    }
  };
  // é chamado uma única vez antes de  iniciar os testes
  beforeAll(() => {
    spyOn(Calculadora, "somar");
  });

  // importante do objeto spy quando é criado, já é inicializado automaticamente como undefined
  it('deve chamar o método somar pelo menos 1 vez', () => {
    // chama o método
    Calculadora.somar(1,1);
    Calculadora.somar(2,2);
    expect(Calculadora.somar).toHaveBeenCalledWith(1,1);
    expect(Calculadora.somar).toHaveBeenCalledWith(2,2); 
  });
});

/**
  * and.callThrough serve para informar ao spy que o método original deve ser executado.
  * and.callThrough deve ser aplicado ao spy
  * Neste caso o método original será executado, e todos os recursos do spy serão mantido e estarão disponíveis para a aplicação
  */

 describe('and.callThrough', () => {
  //mock sem o spyOn ele retornaria o valor
  let Calculadora = {
    somar: (n1, n2) => {
      return n1 + n2;
    },
    subtrair: (n1, n2) => {
      return n1 - n2;
    }
  };
  // é chamado uma única vez antes de  iniciar os testes
  beforeAll(() => {
    spyOn(Calculadora, "somar").and.callThrough();
    spyOn(Calculadora, "subtrair");
  });

  // importante do objeto spy quando é criado, já é inicializado automaticamente como undefined
  it('deve chamar o método somar pelo menos 1 vez', () => {
    expect(Calculadora.somar(1,1)).toEqual(2); // agora posso testar o resultado, mesmo sendo um spy
    // chamando o método para testes do spy
    Calculadora.somar(1,1)
    expect(Calculadora.somar).toHaveBeenCalled(); // posso continuar usando as implementações do spy
    expect(Calculadora.subtrair(2,2)).toBeUndefined();
  });
});


/**
  * and.returnValues serve para informar ao spy quais os valores a serem retornados por chamada
  * Aceita por parâmetro 1 ou mais valores separados por vírgulas.
  * Se o número de chamada for maior que o número de valores a serem retornados e retornado será undefined.
  * and.returnValues deve ser aplicado ao objeto spy
  */

 describe('and.returnValue', () => {
  //mock sem o spyOn ele retornaria o valor
  let Calculadora = {
    somar: (n1, n2) => {
      return n1 + n2;
    },
    subtrair: (n1, n2) => {
      return n1 - n2;
    }
  };
  // é chamado uma única vez antes de  iniciar os testes
  beforeAll(() => {
    spyOn(Calculadora, "somar").and.returnValue(10); // informo que o valor retornado será 10
    spyOn(Calculadora, "subtrair");
  });

  // importante do objeto spy quando é criado, já é inicializado automaticamente como undefined
  it('deve chamar o método somar pelo menos 1 vez', () => {
    expect(Calculadora.somar(1,2)).toEqual(10); // valido que o recebido foi 10, mesmo entando gerar o erro com a soma de 1+2
  });
});

/**
  * and.returnValues serve para informar ao spy o valor de retorno de um determinado método
  * Deve ser aplicado ao objeto spy 
  * Talvez uma boa aplicação de uso, seria , para testes de valores recebidos de API's
  */

 describe('and.returnValues', () => {
  //mock sem o spyOn ele retornaria o valor
  let Calculadora = {
    somar: (n1, n2) => {
      return n1 + n2;
    },
    subtrair: (n1, n2) => {
      return n1 - n2;
    }
  };
  // é chamado uma única vez antes de  iniciar os testes
  beforeAll(() => {
    spyOn(Calculadora, "somar").and.returnValues(10, 20, 30); // informo que os valores retornados serão 10, 20, 30
    spyOn(Calculadora, "subtrair");
  });

  
  it('deve executar 3 chamdas do and.returnValues', () => {
    expect(Calculadora.somar(1,2)).toEqual(10); // valido que o recebido foi 10, mesmo entando gerar o erro com a soma de 1+2
    expect(Calculadora.somar(1,2)).toEqual(20); // valido que o recebido foi 20, mesmo entando gerar o erro com a soma de 1+2
    expect(Calculadora.somar(1,2)).toEqual(30); // valido que o recebido foi 30, mesmo entando gerar o erro com a soma de 1+2
    expect(Calculadora.somar(1,2)).toBeUndefined();
    expect(Calculadora.somar).toHaveBeenCalledTimes(4); // valido que tem apenas 4 chamadas do spy
  });
});


/**
  * and.callFake serve para definir uma nova implementação para um método de um spy.
  * Deve ser aplicado ao método spy
  * and.callFake recebe um parâmetro uma função como nova implementação a ser executada quando o método é chamado
  */

 describe('and.callFake', () => {
  //mock sem o spyOn ele retornaria o valor
  let Calculadora = {
    somar: (n1, n2) => {
      return n1 + n2;
    },
    subtrair: (n1, n2) => {
      return n1 - n2;
    }
  };
  // é chamado uma única vez antes de  iniciar os testes
  beforeAll(() => {
    spyOn(Calculadora, "somar").and.callFake(
      function multiplicar(n1,n2) {
      return n1 * n2;
    });
    spyOn(Calculadora, "subtrair");
  });

  it('deve retornar 6 na multiplicação do and.callFake', () => {
    expect(Calculadora.somar(2,3)).toEqual(6); 
  });
});



/**
  * and.throwError serve para informar ao spy que determinado deve lançar um erro ao ser executado.
  * and.throwError deve ser aplicado ao objeto spy.
  * and.throwError recebe como parâmetro uma string, contendo a mensagem de erro a ser lançada
  */

 describe('and.throwError', () => {
  //mock sem o spyOn ele retornaria o valor
  let Calculadora = {
    somar: (n1, n2) => {
      return n1 + n2;
    },
    subtrair: (n1, n2) => {
      return n1 - n2;
    }
  };
  // é chamado uma única vez antes de  iniciar os testes
  beforeAll(() => {
    spyOn(Calculadora, "somar").and.throwError('Error ao chamar a função');
    spyOn(Calculadora, "subtrair");
  });

  it('deve lançar um erro ao chamar a função soma', () => {
    //OBS: sempre que for usar o throw, deve pasasr uma function no expect
    expect(()=>{Calculadora.somar(1,1)}).toThrowError('Error ao chamar a função'); // validando a string 'Error ao chamar a função' 
  });
});


/**
  * calls.any, todo spy possui um atributo "calls", com informações sobre suas chamadas.  
  * o calls.any, serve para indicar se o método spy, foi chamado ao menos uma vez.
  * Ele não recebe parâmetros e retorna um valor booleano, se ocorreu ou não a chamada do metodo spy
  */
 
// ///// Apresentou problema com o .any, do expect

//  describe('calls.any', () => {
//   //mock sem o spyOn ele retornaria o valor
//   let Calculadora = {
//     somar: (n1, n2) => {
//       return n1 + n2;
//     },
//     subtrair: (n1, n2) => {
//       return n1 - n2;
//     }
//   };
//   // é chamado uma única vez antes de  iniciar os testes
//   beforeAll(() => {
//     spyOn(Calculadora, "somar").and.returnValue(10);
//     spyOn(Calculadora, "subtrair");
//   });

//   it('deve mostrar o uso do metodo call.any', () => {
//     // primeiro chamar o método
//     Calculadora.somar(1,1);
//     expect(Calculadora.somar.calls.any()).toBeTruthy()
//   });
// });


/**                                            
 *        ------------- OBJETOS JASMINE -------------      */                                               


/**
 * createSpy é uma função global javascript do jasmine.
 * Serve para criar spies do zero.
 * Ele possue todos os atributos de um objeto spy comum.
 * Recebe como parâmetro o nome da função a ser criado o spy.
 * Deve ser utlizado quando precisa de um objeto que não se tem acesso direto a ele.
 * createSpy, tem a limitação de não permitir implementar o método declarado, assim permite somente a criação do método.
 */

 describe('jasmine.createSpy', () => {
   let somar;

   beforeAll(() => {
     somar = jasmine.createSpy('somar');
   });

   it('deve demonstrar o uso do jasmine.createSpy', () => {
     somar(1,2);                                // sempre chamar o método a ser usado
     expect(somar).toHaveBeenCalled();          // verifica se foi chamado
     expect(somar).toHaveBeenCalledWith(1, 2);  // verifica se tem os mesmos parâmetros
   });
 });


/**
 * createSpyObj é uma função global javascript do jasmine.
 * Serve para criar objeto spy do zero.
 * Ele possue todos os atributos de um objeto spy comum.
 * Recebe como parâmetro o nome do objeto a ser criado, assim como seus métodos em formato de array.
 * Deve ser utlizado quando precisa de um objeto que não se tem acesso direto a ele.
 * createSpyObj, permite a utilização de todos os ".and" visto anteriormente
 */

describe('jasmine.createSpyObj', () => {
  let Calculadora; // o ideal é que seja criado a variável com nome maiúsculo, por causa do objeto, isso ajuda (inclusive) no intelisense da IDE 

  beforeAll(() => {
    Calculadora = jasmine.createSpyObj('calculadora', ['somar','subtrair']);
    Calculadora.somar.and.returnValue(5);
  });

  it('deve demonstrar o uso do jasmine.createSpyObj', () => {
    Calculadora.somar(1,2);
    expect(Calculadora.somar).toHaveBeenCalled();
    expect(Calculadora.somar(1,2)).toEqual(5)
  });
});